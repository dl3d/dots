# That's a do(n) dada.

_ !!! Experimental !!! _

At the time of this writing, I'm using this repo to test different set-ups for shell environments. Looking at ...

- display & [ frame ] [ window ] [ pane] targets / management
- log-ins & remote connections
- nesting rules ( e.g., tmux :: emacs :: eshell : zsh )

At the outset of the project (November 2015), I'm using

- ZSH 5.0.2
- GNU Emacs 25.1 && 24.5
- tmux 1.9

_HT_

[Oh-My-Zsh](http://ohmyz.sh/) & [Antigen](http://antigen.sharats.me/)
[zplug](https://github.com/b4b4r07/zplug) // [zplug2](https://github.com/b4b4r07/zplug2) // [prezto](https://github.com/sorin-ionescu/prezto)

[Vundle](https://github.com/VundleVim/Vundle.vim) & [pathogen](https://github.com/tpope/vim-pathogen)
[vim-plug](https://github.com/junegunn/vim-plug/wiki/faq)

_THX_

[Howard Abrams](http://howardism.org/)
[Aaron Bieber](http://blog.aaronbieber.com/)
[Rick Dillon](https://killring.org/hack-emacs/)

_!!! Follow these pros for more on emacs !!!_

Also check out [unixorn/awesome-zsh-plugins](https://travis-ci.org/unixorn/awesome-zsh-plugins) for more on zsh.

_etc..._

This post offers a nice rundown of Halcyon setup & project management with cabal.

[Setting Up a Haskell Dev Environment](http://dzackgarza.com/tutorials/2015/05/30/setting-up-a-haskell-dev-environment/)

_TODO_

Add a few of those devtools to `ghci`.

[Bash initialization files](http://www.solipsys.co.uk/new/BashInitialisationFiles.html):
