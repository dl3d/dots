#!/usr/bin/zsh
# path to oh-my-zsh installation.
  export ZSH=/home/dled/.oh-my-zsh

# factory OMZ settings
ZSH_THEME="powerlevel9k/powerlevel9k"
CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
HIST_STAMPS="yyyy-mm-dd"

# custom/{plugins,themes}
ZSH_CUSTOM=$ZSH/custom

# plugins
plugins=(
  compleat
  git
  jsontools
  rails
  ruby
  python
  tmux
)

# alias
source $HOME/.aliasrc

# OMZ
source $ZSH/oh-my-zsh.sh
